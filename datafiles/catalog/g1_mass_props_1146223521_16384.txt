// S/C Mass Properties used for g1 segment with t0 = 1146223521
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00178	0.00201	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.843	-6.76
-0.843	 239	-2.82
-6.76	-2.82	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.16	-0.217
-1.16	 240	-2.82
-0.216	-2.82	 217
