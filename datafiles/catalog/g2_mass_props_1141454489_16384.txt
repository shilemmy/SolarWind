// S/C Mass Properties used for g2 segment with t0 = 1141454489
// SC mass (kg)
 424
// SC center of mass in mechanical frame (x,y,z) in meters
0.000746	0.00279	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.779	-6.58
-0.779	 239	-2.97
-6.58	-2.97	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.22	-0.00858
-1.22	 240	-2.97
-0.0084	-2.97	 217
