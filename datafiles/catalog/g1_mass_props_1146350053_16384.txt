// S/C Mass Properties used for g1 segment with t0 = 1146350053
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00182	0.00198	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.846	-6.77
-0.846	 239	-2.82
-6.77	-2.82	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.16	-0.225
-1.16	 240	-2.82
-0.225	-2.82	 217
