// S/C Mass Properties used for g2 segment with t0 = 1143812837
// SC mass (kg)
 424
// SC center of mass in mechanical frame (x,y,z) in meters
0.00126	0.0024	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.811	-6.67
-0.811	 239	-2.9
-6.67	-2.9	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.19	-0.112
-1.19	 240	-2.9
-0.112	-2.9	 217
