// S/C Mass Properties used for g1 segment with t0 = 1150229325
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00264	0.00204	0.491
// SC moment of inertia in body frame in kg-m^2
 121	-1.5	-3.1
-1.5	 121	-2.52
-3.1	-2.52	 202
// SC moment of inertia in H1 frame in kg-m^2
 224	-1.34	-6.86
-1.34	 238	-2.91
-6.86	-2.91	 216
// SC moment of inertia in H2 frame in kg-m^2
 224	-1.67	-0.337
-1.67	 239	-2.91
-0.337	-2.91	 217
