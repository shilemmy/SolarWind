// S/C Mass Properties used for g1 segment with t0 = 1144733557
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00146	0.00225	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.823	-6.7
-0.823	 239	-2.87
-6.7	-2.87	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.18	-0.151
-1.18	 240	-2.87
-0.151	-2.87	 217
