// S/C Mass Properties used for g2 segment with t0 = 1144075125
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.0013	0.00237	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.814	-6.68
-0.814	 239	-2.89
-6.68	-2.89	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.19	-0.12
-1.19	 240	-2.89
-0.12	-2.89	 217
