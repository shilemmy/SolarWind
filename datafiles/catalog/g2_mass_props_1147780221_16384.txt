// S/C Mass Properties used for g2 segment with t0 = 1147780221
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00212	0.00175	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.864	-6.82
-0.864	 239	-2.77
-6.82	-2.77	 216
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.14	-0.285
-1.14	 240	-2.77
-0.285	-2.77	 217
