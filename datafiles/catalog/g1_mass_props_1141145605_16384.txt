// S/C Mass Properties used for g1 segment with t0 = 1141145605
// SC mass (kg)
 424
// SC center of mass in mechanical frame (x,y,z) in meters
0.000677	0.00284	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.775	-6.56
-0.775	 239	-2.98
-6.56	-2.98	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.23	0.00538
-1.23	 240	-2.98
0.00555	-2.98	 217
