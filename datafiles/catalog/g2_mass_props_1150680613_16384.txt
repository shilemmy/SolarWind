// S/C Mass Properties used for g2 segment with t0 = 1150680613
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00273	0.00213	0.491
// SC moment of inertia in body frame in kg-m^2
 121	-1.5	-3.1
-1.5	 121	-2.52
-3.1	-2.52	 202
// SC moment of inertia in H1 frame in kg-m^2
 224	-1.34	-6.88
-1.34	 238	-2.93
-6.88	-2.93	 216
// SC moment of inertia in H2 frame in kg-m^2
 224	-1.67	-0.354
-1.67	 239	-2.93
-0.354	-2.93	 217
