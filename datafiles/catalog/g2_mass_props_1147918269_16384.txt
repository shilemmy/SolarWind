// S/C Mass Properties used for g2 segment with t0 = 1147918269
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00216	0.00172	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.867	-6.83
-0.867	 239	-2.77
-6.83	-2.77	 216
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.14	-0.293
-1.14	 240	-2.77
-0.293	-2.77	 217
