// S/C Mass Properties used for g1 segment with t0 = 1145759353
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00171	0.00206	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.839	-6.75
-0.839	 239	-2.83
-6.75	-2.83	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.17	-0.202
-1.17	 240	-2.83
-0.202	-2.83	 217
