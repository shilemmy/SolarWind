// S/C Mass Properties used for g1 segment with t0 = 1149346105
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00247	0.00184	0.491
// SC moment of inertia in body frame in kg-m^2
 121	-1.5	-3.1
-1.5	 121	-2.52
-3.1	-2.52	 202
// SC moment of inertia in H1 frame in kg-m^2
 224	-1.36	-6.83
-1.36	 238	-2.87
-6.83	-2.87	 216
// SC moment of inertia in H2 frame in kg-m^2
 224	-1.65	-0.303
-1.65	 239	-2.87
-0.303	-2.87	 217
