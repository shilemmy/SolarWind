// S/C Mass Properties used for g1 segment with t0 = 1145167037
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00158	0.00216	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.831	-6.73
-0.831	 239	-2.85
-6.73	-2.85	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.18	-0.176
-1.18	 240	-2.85
-0.175	-2.85	 217
