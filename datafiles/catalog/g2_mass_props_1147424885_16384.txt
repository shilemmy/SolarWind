// S/C Mass Properties used for g2 segment with t0 = 1147424885
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00207	0.00179	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.861	-6.81
-0.861	 239	-2.78
-6.81	-2.78	 216
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.15	-0.275
-1.15	 240	-2.78
-0.275	-2.78	 217
