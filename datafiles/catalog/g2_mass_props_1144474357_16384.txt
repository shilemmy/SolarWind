// S/C Mass Properties used for g2 segment with t0 = 1144474357
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.0014	0.0023	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.82	-6.69
-0.82	 239	-2.88
-6.69	-2.88	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.19	-0.14
-1.19	 240	-2.88
-0.14	-2.88	 217
