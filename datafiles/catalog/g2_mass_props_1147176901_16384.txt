// S/C Mass Properties used for g2 segment with t0 = 1147176901
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.002	0.00184	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.857	-6.8
-0.857	 239	-2.79
-6.8	-2.79	 216
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.15	-0.261
-1.15	 240	-2.79
-0.26	-2.79	 217
