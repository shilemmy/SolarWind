// S/C Mass Properties used for g1 segment with t0 = 1142564885
// SC mass (kg)
 424
// SC center of mass in mechanical frame (x,y,z) in meters
0.00101	0.00259	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.796	-6.62
-0.796	 239	-2.93
-6.62	-2.93	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.21	-0.0614
-1.21	 240	-2.93
-0.0613	-2.93	 217
