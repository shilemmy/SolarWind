// S/C Mass Properties used for g2 segment with t0 = 1143384661
// SC mass (kg)
 424
// SC center of mass in mechanical frame (x,y,z) in meters
0.00115	0.00249	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.804	-6.65
-0.804	 239	-2.91
-6.65	-2.91	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.2	-0.0894
-1.2	 240	-2.91
-0.0892	-2.91	 217
