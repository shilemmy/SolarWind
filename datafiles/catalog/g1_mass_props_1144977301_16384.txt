// S/C Mass Properties used for g1 segment with t0 = 1144977301
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00153	0.0022	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.828	-6.72
-0.828	 239	-2.86
-6.72	-2.86	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.18	-0.165
-1.18	 240	-2.86
-0.165	-2.86	 217
