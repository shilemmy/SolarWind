// S/C Mass Properties used for g2 segment with t0 = 1149579537
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00253	0.0019	0.491
// SC moment of inertia in body frame in kg-m^2
 121	-1.5	-3.1
-1.5	 121	-2.52
-3.1	-2.52	 202
// SC moment of inertia in H1 frame in kg-m^2
 224	-1.35	-6.84
-1.35	 238	-2.88
-6.84	-2.88	 216
// SC moment of inertia in H2 frame in kg-m^2
 224	-1.66	-0.314
-1.66	 239	-2.88
-0.314	-2.88	 217
