// S/C Mass Properties used for g1 segment with t0 = 1148340021
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00224	0.00166	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.871	-6.84
-0.871	 239	-2.76
-6.84	-2.76	 216
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.14	-0.308
-1.14	 240	-2.76
-0.308	-2.76	 217
