// S/C Mass Properties used for g1 segment with t0 = 1142292021
// SC mass (kg)
 424
// SC center of mass in mechanical frame (x,y,z) in meters
0.000935	0.00265	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.791	-6.61
-0.791	 239	-2.95
-6.61	-2.95	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.21	-0.0465
-1.21	 240	-2.95
-0.0463	-2.95	 217
