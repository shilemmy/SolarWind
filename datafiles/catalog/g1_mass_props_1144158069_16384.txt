// S/C Mass Properties used for g1 segment with t0 = 1144158069
// SC mass (kg)
 423
// SC center of mass in mechanical frame (x,y,z) in meters
0.00135	0.00234	0.491
// SC moment of inertia in body frame in kg-m^2
 122	  -1	-3.15
  -1	 121	-2.44
-3.15	-2.44	 202
// SC moment of inertia in H1 frame in kg-m^2
 225	-0.817	-6.69
-0.817	 239	-2.89
-6.68	-2.89	 217
// SC moment of inertia in H2 frame in kg-m^2
 225	-1.19	-0.13
-1.19	 240	-2.89
-0.13	-2.89	 217
